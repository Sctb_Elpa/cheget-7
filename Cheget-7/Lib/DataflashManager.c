/*
             LUFA Library
     Copyright (C) Dean Camera, 2014.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2014  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

/** \file
 *
 *  Functions to manage the physical Dataflash media, including reading and writing of
 *  blocks of data. These functions are called by the SCSI layer when data must be stored
 *  or retrieved to/from the physical storage media. If a different media is used (such
 *  as a SD card or EEPROM), functions similar to these will need to be generated.
 */

#define  INCLUDE_FROM_DATAFLASHMANAGER_C

#include <avr/wdt.h>

#include "DataflashManager.h"
#include "24cXXX.h"
#include "debug.h"

#define ENDPOINT_IO_ONCE		16

/** Writes blocks (OS blocks, not Dataflash pages) to the storage medium, the board Dataflash IC(s), from
 *  the pre-selected data OUT endpoint. This routine reads in OS sized blocks from the endpoint and writes
 *  them to the Dataflash in Dataflash page sized blocks.
 *
 *  \param[in] MSInterfaceInfo  Pointer to a structure containing a Mass Storage Class configuration and state
 *  \param[in] BlockAddress  Data block starting address for the write sequence
 *  \param[in] TotalBlocks   Number of blocks of data to write
 */
void DataflashManager_WriteBlocks(USB_ClassInfo_MS_Device_t* const MSInterfaceInfo,
                                  const uint32_t BlockAddress,
                                  uint16_t TotalBlocks)
{
	uint32_t curentBlock = BlockAddress;
	
	/* Wait until endpoint is ready before continuing */
	if (Endpoint_WaitUntilReady())
		return;
	
	//DEBUG_MSG("W: %li, c=%i\t", (long)curentBlock, (int)TotalBlocks);
	
	while (TotalBlocks)
	{
#if USE_WDT == 1
		wdt_reset();
#endif
		uint8_t Buffer[DATAFLASH_PAGE_SIZE];
		
		// ����� ������ -> �� ������ �������� ����� �� ������
		for (uint8_t page = 0; page < VIRTUAL_MEMORY_BLOCK_SIZE / DATAFLASH_PAGE_SIZE; ++page)
		{
			for (uint8_t chank = 0; chank < DATAFLASH_PAGE_SIZE / ENDPOINT_IO_ONCE; ++chank)
			{
				if (!(Endpoint_IsReadWriteAllowed()))
				{
					/* Clear the current endpoint bank */
					Endpoint_ClearOUT();
			
					/* Wait until the host has sent another packet */
					if (Endpoint_WaitUntilReady())
						return;
				}
				
				for (uint16_t i = 0; i < ENDPOINT_IO_ONCE; ++i)
					Buffer[chank * ENDPOINT_IO_ONCE + i] = Endpoint_Read_8();
			
				if (MSInterfaceInfo->State.IsMassStoreReset)
					return;
			}
			
			EEWriteBytes(curentBlock * VIRTUAL_MEMORY_BLOCK_SIZE + page * DATAFLASH_PAGE_SIZE, Buffer, DATAFLASH_PAGE_SIZE);
		}
		
		/* Decrement the blocks remaining counter and reset the sub block counter */
		curentBlock++;
		TotalBlocks--;
	}

	/* If the endpoint is empty, clear it ready for the next packet from the host */
	if (!(Endpoint_IsReadWriteAllowed()))
		Endpoint_ClearOUT();
}

/** Reads blocks (OS blocks, not Dataflash pages) from the storage medium, the board Dataflash IC(s), into
 *  the pre-selected data IN endpoint. This routine reads in Dataflash page sized blocks from the Dataflash
 *  and writes them in OS sized blocks to the endpoint.
 *
 *  \param[in] MSInterfaceInfo  Pointer to a structure containing a Mass Storage Class configuration and state
 *  \param[in] BlockAddress  Data block starting address for the read sequence
 *  \param[in] TotalBlocks   Number of blocks of data to read
 */
void DataflashManager_ReadBlocks(USB_ClassInfo_MS_Device_t* const MSInterfaceInfo,
                                 const uint32_t BlockAddress,
                                 uint16_t TotalBlocks)
{
	uint32_t curentBlock = BlockAddress;
	
	/* Wait until endpoint is ready before continuing */
	if (Endpoint_WaitUntilReady())
		return;
		
	//DEBUG_MSG("R: %li, c=%i\t", (long)curentBlock, (int)TotalBlocks);
	
	while (TotalBlocks)
	{
#if USE_WDT == 1
		wdt_reset();
#endif
		/* Read a data block from the SD card */
	
		for (uint16_t chank = 0; chank < VIRTUAL_MEMORY_BLOCK_SIZE / ENDPOINT_IO_ONCE; ++chank)
		{
			uint8_t Buffer[ENDPOINT_IO_ONCE];
			
			EEReadBytes(Buffer, curentBlock * VIRTUAL_MEMORY_BLOCK_SIZE + chank * ENDPOINT_IO_ONCE, ENDPOINT_IO_ONCE);
						
			if (!(Endpoint_IsReadWriteAllowed()))
			{
				/* Clear the endpoint bank to send its contents to the host */
				Endpoint_ClearIN();
			
				/* Wait until the endpoint is ready for more data */
				if (Endpoint_WaitUntilReady())
					return;
			}
			
			for (uint16_t i = 0; i < ENDPOINT_IO_ONCE; ++i)
				Endpoint_Write_8(Buffer[i]);
				
			if (MSInterfaceInfo->State.IsMassStoreReset)
				return;
		}
		
		/* Decrement the blocks remaining counter */
		curentBlock++;
		TotalBlocks--;
	}
	
	/* If the endpoint is full, send its contents to the host */
	if (!(Endpoint_IsReadWriteAllowed()))
		Endpoint_ClearIN();
}

/** Writes blocks (OS blocks, not Dataflash pages) to the storage medium, the board Dataflash IC(s), from
 *  the given RAM buffer. This routine reads in OS sized blocks from the buffer and writes them to the
 *  Dataflash in Dataflash page sized blocks. This can be linked to FAT libraries to write files to the
 *  Dataflash.
 *
 *  \param[in] BlockAddress  Data block starting address for the write sequence
 *  \param[in] TotalBlocks   Number of blocks of data to write
 *  \param[in] BufferPtr     Pointer to the data source RAM buffer
 */
void DataflashManager_WriteBlocks_RAM(const uint32_t BlockAddress,
                                      uint16_t TotalBlocks,
                                      uint8_t* BufferPtr)
{
#if USE_WDT == 1
		wdt_reset();
#endif	
	for (	uint16_t addr = BlockAddress * VIRTUAL_MEMORY_BLOCK_SIZE;
			addr < (BlockAddress + TotalBlocks) * VIRTUAL_MEMORY_BLOCK_SIZE;
			addr += DATAFLASH_PAGE_SIZE, BufferPtr += DATAFLASH_PAGE_SIZE
		)
		{	
			EEWriteBytes(addr, BufferPtr, DATAFLASH_PAGE_SIZE);
		}
}


/** Reads blocks (OS blocks, not Dataflash pages) from the storage medium, the board Dataflash IC(s), into
 *  the preallocated RAM buffer. This routine reads in Dataflash page sized blocks from the Dataflash
 *  and writes them in OS sized blocks to the given buffer. This can be linked to FAT libraries to read
 *  the files stored on the Dataflash.
 *
 *  \param[in] BlockAddress  Data block starting address for the read sequence
 *  \param[in] TotalBlocks   Number of blocks of data to read
 *  \param[out] BufferPtr    Pointer to the data destination RAM buffer
 */
void DataflashManager_ReadBlocks_RAM(const uint32_t BlockAddress,
                                     uint16_t TotalBlocks,
                                     uint8_t* BufferPtr)
{
#if USE_WDT == 1
	wdt_reset();
#endif
	for (	uint16_t addr = BlockAddress * VIRTUAL_MEMORY_BLOCK_SIZE; 
			addr < (BlockAddress + TotalBlocks) * VIRTUAL_MEMORY_BLOCK_SIZE; 
			addr += DATAFLASH_PAGE_SIZE, BufferPtr += DATAFLASH_PAGE_SIZE
		)
		{
			EEReadBytes(BufferPtr, addr, DATAFLASH_PAGE_SIZE);
		}
}

/** Disables the Dataflash memory write protection bits on the board Dataflash ICs, if enabled. */
void DataflashManager_ResetDataflashProtections(void)
{
}

/** Performs a simple test on the attached Dataflash IC(s) to ensure that they are working.
 *
 *  \return Boolean \c true if all media chips are working, \c false otherwise
 */
bool DataflashManager_CheckDataflashOperation(void)
{
	return true;
}
