#include <avr/io.h>
#include <util/delay.h>

#include "24cXXX.h"
#include <LUFA/Drivers/Board/Dataflash.h>

char EEBegin(uint16_t addr)
{
	if (!i2c_start(EEPROM_ADDRESS(EEPROM_WRITE)))
		return 0;
	i2c_write(addr >> 8);
	i2c_write((uint8_t)addr);
	return 1;
}

void EEWriteByte(uint16_t address, uint8_t data)
{
	while (!EEBegin(address))
		i2c_stop();
	i2c_write(data);
	i2c_stop();
}

void EEWriteBytes(uint16_t dest_addr, uint8_t *__src, uint16_t size)
{	
	if (size > DATAFLASH_PAGE_SIZE) // ������ ������ ������ ��� 128 ���� �� ���, �� � ������ �����
		return;

	while (!EEBegin(dest_addr))
		i2c_stop();
	
	while(size--)
	{
		i2c_write(*__src);
		++__src;
	}
	i2c_stop();
}

uint8_t EEReadByte(uint16_t address)
{
	while (!EEBegin(address))
		i2c_stop();
	
	//read
	i2c_rep_start(EEPROM_ADDRESS(EEPROM_READ));
	return i2c_readNak();
}

void EEReadBytes(uint8_t* buf, uint16_t address, uint16_t size)
{
	// ���� �����������, ��� ��� ������ �������� ����� �� ��������� � ��������� ��������
	// �� � ��� ��������, ��� ��� ���� � ������� ���
	
	while (!EEBegin(address))
		i2c_stop();
	i2c_rep_start(EEPROM_ADDRESS(EEPROM_READ));
	while (size-- > 1)
	{
		*buf = i2c_readAck();
		++buf;
	}
	*buf = i2c_readNak();
	i2c_stop();	
}

		


