/*
 * Settings.c
 *
 * Created: 29.05.2014 14:17:36
 *  Author: tolyan
 */ 

#include <avr/eeprom.h>

#include "config/AppConfig.h"

#include "Settings.h"

struct sSettings settings EEMEM =
{
	.uart_speed = 19200,
	.regulator_addr = 6,
	.rx_timeout = 15,
	.hysteresis = 0.5,
	.idletemperature = -65.5,
	
	.SimpltModeTarget = 0,
};

uint16_t settings_getu16(int addr)
{
	uint16_t res;
	eeprom_read_block(&res, (void*)&settings + addr, sizeof(uint16_t));
	return res;
}

uint8_t settings_getu8(int addr)
{
	return eeprom_read_byte((uint8_t*)&settings + addr);
}

float settings_getfloat(int addr)
{
	float res;
	eeprom_read_block(&res, (void*)&settings + addr, sizeof(float));
	return res;
}

void settings_putfloat(int addr, float val)
{
	eeprom_write_float((void*)&settings + addr, val);
}