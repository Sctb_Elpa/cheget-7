/*
 * Coils.c
 *
 * Created: 13.07.2015 15:30:28
 *  Author: tolyan
 */ 

#include <avr/io.h>

#include "Coils.h"

#define COLI_NAGR_PORT		PORTC
#define COLI_NAGR_PIN		PINC
#define COLI_NAGR_PINN		(1 << 1)

#define COLI_KOMPR_PORT		PORTC
#define COLI_KOMPR_PIN		PINC
#define COLI_KOMPR_PINN		(1 << 0)

#define IS_INVERTED			0

void Coils_Init(void)
{
#if IS_INVERTED
	COLI_NAGR_PORT |= COLI_NAGR_PIN;
	COLI_KOMPR_PORT |= COLI_KOMPR_PIN;
#else
	COLI_NAGR_PORT &= ~COLI_NAGR_PINN;
	COLI_KOMPR_PORT &= ~COLI_KOMPR_PINN;
#endif	
	DDRC |= COLI_NAGR_PINN | COLI_KOMPR_PINN;
}


void CoilSetState(enum enCoils coil, bool newState)
{
#if IS_INVERTED
	newState = !newState;
#endif
	switch (coil)
	{
	case COIL_NAGR:
		if (newState)
			COLI_NAGR_PORT |= COLI_NAGR_PINN;
		else
			COLI_NAGR_PORT &= ~COLI_NAGR_PINN;
		break;
	case COIL_KOMPR:
		if (newState)
			COLI_KOMPR_PORT |= COLI_KOMPR_PINN;
		else
			COLI_KOMPR_PORT &= ~COLI_KOMPR_PINN;
		break;
	default:
		return;
	}
}

bool CoilGetState(enum enCoils coil)
{
	uint8_t res;
	switch (coil)
	{
		case COIL_NAGR:
			res = COLI_NAGR_PIN & COLI_NAGR_PINN;
			break;
		case COIL_KOMPR:
			res = COLI_KOMPR_PIN & COLI_KOMPR_PINN;
			break;
		default:
			return 0;
	}
	return IS_INVERTED ? !res : (bool)res;	
}