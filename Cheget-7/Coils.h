/*
 * Coils.h
 *
 * Created: 13.07.2015 15:35:09
 *  Author: tolyan
 */ 


#ifndef COILS_H_
#define COILS_H_

#include <stdbool.h>


enum enCoils
{
	COIL_DUMY = 0,
	COIL_NAGR = 1,
	COIL_KOMPR = 2,
};

void Coils_Init(void);
void CoilSetState(enum enCoils coil, bool newState);
bool CoilGetState(enum enCoils coil);


#endif /* COILS_H_ */