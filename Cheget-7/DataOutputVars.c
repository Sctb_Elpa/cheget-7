#include "ds1307.h"
#include "regulator.h"

#include "dataOutputVars.h"


struct my_HID_Report dataOutputVars;


void updateTime(void)
{
	ds1307_getdate(&dataOutputVars.dateTime);
}

void updateCurrent()
{
	float t = getCurrentTemperature();
	// �������!!!
	// ����� ���� ����� ���� ������������ ������, ��� ��� ��� ����� �������
	if (!isnanf(t))
		dataOutputVars.CurrentTemperature = t;
}

void updateTarget()
{
	dataOutputVars.TargetTemperature = getTargetTemperature();
}