/*
 * Termoprogramm.h
 *
 * Created: 26.08.2015 14:18:33
 *  Author: tolyan
 */ 


#ifndef TERMOPROGRAMM_H_
#define TERMOPROGRAMM_H_

#define STEP_SWITCH_DELTA_MAX	(0.1)	// ������������ ������� ��� ��������� ������

#include "UI.h"

enum TermProgStep {
	T_PRG_STEP_IDLE = 0,
	T_PRG_STEP_1	= 1,
	T_PRG_STEP_2	= 2,
	T_PRG_STEP_3	= 3,
	T_PRG_STEP_4	= 4,
	T_PRG_STEP_5	= 5,
	T_PRG_STEP_6	= 6,
	T_PRG_STEP_7	= 7,
	T_PRG_STEP_8	= 8,
	T_PRG_STEP_9	= 9,
	T_PRG_STEP_END	= 10,
	
	T_PRG_STEPS		= T_PRG_STEP_END + 1,
	
	T_SIMPLE		= T_PRG_STEPS + 1
};

enum enStepDoCode {
	STEP_EMPTY = 0,
	STEP_TEMP_UP = UpArrow,
	STEP_TEMP_DOWN = DownArrow,
	STEP_HOLD = '-',
};

struct StepData {
	enum TermProgStep	stepID;						// ID ���� ��� T_PRG_STEP_IDLE, ���� �����
	int32_t				Time;						// ����� ���� (������ ��� ������ ���������) (-1) - ����������
	float				TargetTemperature;			// ������� �����������
	unsigned			isCoolingRequred:1;			// ����� �����������? (>0 - ��) (0 -���) (<0 - �� �������)
	unsigned			isStopCoolingAfterStep:1;	// ���� ����, ��� ���������� ��������� ����������� �� ���������� ����.
	enum enStepDoCode	StepDoCode;					// ��� �������� �� ���� ����
};

struct sTermoprogramm {
	struct StepData *steps;
	uint8_t len;
	void (*callback_stepSwitched)(enum TermProgStep newStep); 
};

struct StepData Tp_getStep(enum TermProgStep step);
uint8_t Tp_setStep(enum TermProgStep step, struct StepData *stepData);
int8_t isCoolingRequred(float StartTemperature, float TargetTemperature);
int8_t isCoolingRequredCS(void);

// ��������� ��� ���� ��������� StepData ���� ������� ������-�����������
void constructTermoprogramm(struct sTermoprogramm* termoprog, float StartTemperature);
void ExecTermoprogramm(struct sTermoprogramm* termoprog);
void BreakTermoprogramm(void);

enum enStepDoCode getActionCode(float prev, float target);
void TermoprogrammTick_1s(void);

#endif /* TERMOPROGRAMM_H_ */