/*
 * Display.h
 *
 * Created: 14.07.2015 10:26:18
 *  Author: tolyan
 */ 


#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <stdint.h>
#include <stdio.h>

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

// flags for backlight control
#define LCD_BACKLIGHT 0x08
#define LCD_NOBACKLIGHT 0x00

struct sDisplayCtrl
{
	uint8_t _Addr;
	uint8_t _displayfunction;
	uint8_t _displaycontrol;
	uint8_t _displaymode;
	uint8_t _numlines;
	uint8_t _cols;
	uint8_t _rows;
	uint8_t _backlightval;
	uint8_t xpos;
	FILE displayout;
};

void Display_constructor(struct sDisplayCtrl* this, uint8_t lcd_Addr,uint8_t lcd_cols,uint8_t lcd_rows, uint8_t backlight);
void Display_init(struct sDisplayCtrl* this, uint8_t dotsize);
void Display_SetCursor(struct sDisplayCtrl* this, uint8_t column, uint8_t row);
void Display_Clear(struct sDisplayCtrl* this);
void Display_putchar(struct sDisplayCtrl* this, uint8_t c);
void Display_home(struct sDisplayCtrl* this);
void Display_displayOn(struct sDisplayCtrl* this);
int Display_printf(struct sDisplayCtrl* this, char* fmt, ...);
int Display_printf_P(struct sDisplayCtrl* this, const char* fmt, ...);
uint8_t Display_currentX(struct sDisplayCtrl* this);
void Display_finishLine(struct sDisplayCtrl* this);

#endif /* DISPLAY_H_ */