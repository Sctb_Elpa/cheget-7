/*
 * filter.c
 *
 * Created: 22.09.2014 8:41:07
 *  Author: tolyan
 */ 

#include <string.h>
#include <stdint.h>
#include <math.h>
#include <avr/pgmspace.h>

#include "filter.h"

float filter(struct sFilter* f, float input)
{
	float coeffs[f->rang];
	memcpy_P(&coeffs, f->coeffs, sizeof(float) * f->rang);
	
	memmove(&f->values[1], &f->values[0], f->rang - 1);
	f->values[0] = input;
	
	float res = 0;
	uint8_t nanCounter = 0;
	for (uint8_t i = 0; i < f->rang; ++i)
	{
		if (isnan(f->values[i]))
			++nanCounter;
		else
			res += coeffs[i] * f->values[i];
	}
	
	if (nanCounter)
		res *= f->rang / (f->rang - nanCounter);		
	return res;
}


void filterReset(struct sFilter* f)
{
	for (uint8_t i = 0; i < f->rang; ++i)
		f->values[i] = NAN;
	f->valuesLoaded = 0;
}
