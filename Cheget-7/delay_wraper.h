/*
 * IncFile1.h
 *
 * Created: 14.05.2014 8:51:12
 *  Author: tolyan
 */ 


#ifndef DALAY_WRAPER_H_
#define DALAY_WRAPER_H_

#ifdef DELAY_WRAPER
/* ������������ ��� ����������� ���� ������� delay_*() � ������ ��������� ���� avoid (__attribute__((always_inline)))*/
void delay_ms(double __ms);
void delay_us(double __us);
#else
 #include <util/delay.h>
 #define delay_ms(t)	_delay_ms(t)
 #define delay_us(t)	_delay_us(t)
#endif // DELAY_WRAPER




#endif /* DALAY_WRAPER_H_ */