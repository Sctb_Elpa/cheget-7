/*
 * UI.c
 *
 * Created: 15.07.2015 9:17:29
 *  Author: tolyan
 */ 

#include <math.h>

#include "regulator.h"
#include "Display.h"
#include "dataOutputVars.h"
#include "ButtonsProcessor.h"
#include "Coils.h"
#include "Termoprogramm.h"

#include "UI.h"

const char Temperature[] = "T";
const char Ustavka[] = {0b10101001, 'c', 0b10111111, 0};
const char Disabled[] = {'B', 0b11000011, 0b10111010, 0b10111011, 0};	
const char degrie[] = {' ', degrie_c, 'C', 0};

extern struct UI_ctr SimpleUI;
extern struct UI_ctr AdvancedUI;

static struct UI_ctr *currentCtrl;

static struct sDisplayCtrl *display;

static BtnCallBack cb_bak;

uint8_t modifyTarget(float* val, float diff)
{
	if (*val == IDLE_TEMPERATURE)
		return 0;
	else
	{
		*val += diff;
		return 1;
	}
}

static void reenableTogglingLight(void)
{
	btns[BTN_SEL].onLongPush = toggleLight;
	btns[BTN_SEL].onRelease = cb_bak;
}

void toggleLight(void)
{
	CoilSetState(COIL_NAGR, !CoilGetState(COIL_NAGR));
	btns[BTN_SEL].onLongPush = NULL;
	cb_bak = btns[BTN_SEL].onRelease;
	btns[BTN_SEL].onRelease = reenableTogglingLight;
}

void UI_Init(void)
{
	currentCtrl->Init();
}

static void switchToSimpleMode(void)
{
	currentCtrl = &SimpleUI;
	UI_Init();
}

static void switchToAdvansedMode(void)
{
	currentCtrl = &AdvancedUI;
	UI_Init();
}

void UI_setDisplay(struct sDisplayCtrl *d)
{
	display = d;
	
	SimpleUI.SwitchModeRequred = switchToAdvansedMode;
	AdvancedUI.SwitchModeRequred = switchToSimpleMode;
	
	switchToSimpleMode();
}

void updateDisplay(void)
{
	currentCtrl->Draw(display);
}

char getStatusFlag(void)
{
	uint8_t ststusFlag = Solid;

	if (dataOutputVars.startStop)
	{
		if (dataOutputVars.dateTime.second % 2)
		{
			if (fabs(dataOutputVars.CurrentTemperature - dataOutputVars.TargetTemperature) <= STEP_SWITCH_DELTA_MAX)
				ststusFlag = ' ';
			else
			{
				if (dataOutputVars.CurrentTemperature > dataOutputVars.TargetTemperature)
					ststusFlag = CoilGetState(COIL_KOMPR) ? kompressor_pic : DownArrow;
				else
					ststusFlag = UpArrow;
			}
				
		}
		else
			ststusFlag = ' ';
	}
	
	return ststusFlag;
}