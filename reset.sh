#!/bin/bash

DIR=`cat *.atsln  | grep Project | cut -d ',' -f2 | grep cproj | cut -d '\' -f 1 | sed 's/\"//' | sed 's/ //'`

BATCHIP="/c/Program Files (x86)/Atmel/Flip 3.4.7/bin/batchisp.exe"
#BATCHIP="c:\Program Files\Atmel\Flip 3.4.7\bin\batchisp.exe"
HEX=$DIR/Debug/$DIR.hex
EPP=$DIR/Debug/$DIR.eep
EPP_NEW=./$DIR.hex
CHIP=at90usb646

echo +++++START NORESET+++++
"$BATCHIP" -hardware usb -device ${CHIP} -operation onfail abort start noreset 0
