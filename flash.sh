#!/bin/bash

DIR=`cat Cheget-7.atsln  | grep Project | cut -d ',' -f2 | grep cproj | cut -d '\' -f 1 | sed -e 's/\"//' -e 's/ //'`

echo "Dir: " $DIR

BATCHIP="/c/Program Files (x86)/Atmel/Flip 3.4.7/bin/batchisp.exe"
#BATCHIP="c:\Program Files\Atmel\Flip 3.4.7\bin\batchisp.exe"
HEX=$DIR/Release/$DIR.hex
EPP=$DIR/Release/$DIR.eep
EPP_NEW=./$DIR.hex
CHIP=at90usb646

cp $EPP $EPP_NEW

echo +++++EREASE+++++
"$BATCHIP" -hardware usb -device ${CHIP} -operation onfail abort erase f

echo +++++FLASH+++++
"$BATCHIP" -hardware usb -device ${CHIP} -operation onfail abort memory flash loadbuffer $HEX program VERIFY

echo +++++EEPROM+++++
"$BATCHIP" -hardware usb -device ${CHIP} -operation onfail abort memory eeprom loadbuffer $EPP_NEW program VERIFY

echo +++++START NORESET+++++
"$BATCHIP" -hardware usb -device ${CHIP} -operation onfail abort start noreset 0

rm $EPP_NEW